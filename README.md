# Notas

- http://www.mkyong.com/spring-boot/spring-boot-spring-security-thymeleaf-example/

- La autenticación se hace en el footer (isAuthenticated())

- Definición de nivel de log por paquete (Logback está por defecto) en applications.properties, por ejemplo 
  logging.level.com.privalia = DEBUG
  
- AccessDeniedHandler 